my_set = {1, 2, 3, 4, 5}
print(len(my_set))
my_set.add(6)
print(my_set)
my_set.remove(3)
print(my_set)
my_set.discard(2)
print(my_set)
item = my_set.pop()
print(item)
print(my_set)
my_set.clear()
print(my_set)

