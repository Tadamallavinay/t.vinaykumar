
my_list = [1, 2, 3, 4, 5]

print(len(my_list))  # Output: 5

my_list.append(6)
print(my_list)  # Output: [1, 2, 3, 4, 5, 6]

my_list.insert(2, 7)
print(my_list)  # Output: [1, 2, 7, 3, 4, 5, 6]

my_list.remove(3)
print(my_list)  # Output: [1, 2, 7, 4, 5, 6]

item = my_list.pop(4)
print(item)
print(my_list)

index = my_list.index(7)
print(index)

my_list.sort()
print(my_list)

my_list.reverse()
print(my_list)
